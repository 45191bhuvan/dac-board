import react from 'react';
import './Contact.css';
import bhuvan from '../photo/bhuvan.jpg';
import venkatesh from '../photo/venkatesh.jpg';
import rutuja from '../photo/rutuja.jpg';
import suresh from '../photo/suresh.jpg';
import aniket from '../photo/aniket.jpg';
import shweta from '../photo/shweta.jpg';
const Contact = props => {
  return (
    <>
      <div >
      <br></br>
        <h1 style={{fontWeight:"bold"}}>Contact Us</h1><br />
        
     
        <div className="container-fluid d-flex justify-content-center">
          <div className="row" >

            <div className="card text-center mb-2 mr-2 class="border-dark >
              <div className="overflow">
                <img src={suresh} alt="" className="card-img-top" />
              </div>
              <div className="card-body text-dark">
                <h4 className="card-title">Suresh Date</h4>
                <p className="card-text text-secondary">
                  <dd>sureshdate1996@gmail.com
</dd>
                  <dd></dd>
                </p>
              </div>
            </div>

            <div className="card text-center mb-2 mr-2">
              <div className="overflow">
                <img src={venkatesh} alt="" className="card-img-top" />
              </div>
              <div className="card-body text-dark">
                <h4 className="card-title">Venkatesh Joshi</h4>
                <p className="card-text text-secondary">
                  <dd>joshivenkatesh1996@gmail.com</dd>
                  <dd></dd>
                </p>
              </div>
            </div>

            <div className="card text-center mb-2 mr-2">
              <div className="overflow">
                <img src={bhuvan} alt="" className="card-img-top" />
              </div>
              <div className="card-body text-dark">
                <h4 className="card-title">Bhuvan Patil</h4>
                <p className="card-text text-secondary">
                  <dd>bhuvanpatil41@gmail.com
</dd>
                  <dd></dd>
                </p>
              </div>
            </div>

          </div>
        </div>

        <div className="container-fluid d-flex justify-content-center">
          <div className="row  mt-3">

            <div className="card text-center mr-2">
              <div className="overflow">
                <img src={aniket} alt="" className="card-img-top" />
              </div>
              <div className="card-body text-dark">
                <h4 className="card-title">Aniket Vyavhare</h4>
                <p className="card-text text-secondary">
                  <dd>aniketvyavhare99@gmail.com
</dd>
                  <dd></dd>
                </p>
              </div>
            </div>

            <div className="card text-center mr-2">
              <div className="overflow">
                <img src={shweta} alt="" className="card-img-top" />
              </div>
              <div className="card-body text-dark">
                <h4 className="card-title">Shweta bhosale</h4>
                <p className="card-text text-secondary">
                  <dd>shwetabhosale30@gmail.com
</dd>
                  <dd></dd>
                </p>
              </div>
            </div>

            <div className="card text-center">
              <div className="overflow">
                <img src={rutuja} alt="" className="card-img-top" />
              </div>
              <div className="card-body text-dark">
                <h4 className="card-title">Rutuja Patil</h4>
                <p className="card-text text-secondary">
                  <dd>patilrutuja597@gmail.com
</dd>
                  <dd></dd>
                </p>
              </div>
            </div>

          </div>
        </div>
        <br></br>
        <br></br>
        <br></br>
      </div>
    </>
  );
}

export default Contact;